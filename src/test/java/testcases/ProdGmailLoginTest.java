package testcases;

import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.trainerwebchecklist.qa.actions.CloseBrowserAction;
import com.trainerwebchecklist.qa.actions.GmailAction;
import com.trainerwebchecklist.qa.helpers.Browserhelper;
import com.trainerwebchecklist.qa.testdata.LoginDataProvider;
import com.trainerwebchecklist.qa.utilities.Log;




public class ProdGmailLoginTest {
	
	static WebDriver driver;
	
@BeforeClass
public void OpenBrowser() throws Exception {	
	PropertyConfigurator.configure("log4j.properties");
	driver = Browserhelper.openProdBrowser();}
	
@Test (dataProvider = "GmailLogin" , dataProviderClass = LoginDataProvider.class)
public static void ProductionGmailLogin(String Gmailid,String GmailPswd) throws Exception{	
	GmailAction.socialGmailProdLogin(driver, Gmailid, GmailPswd);
	//LogoutAction.UserLogOut(driver);
	}

@AfterClass
public void teardown() throws Exception{	
	CloseBrowserAction.Application(driver);}
}
