package testcases;

import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.trainerwebchecklist.qa.actions.CloseBrowserAction;
import com.trainerwebchecklist.qa.actions.LoginAction;
import com.trainerwebchecklist.qa.helpers.Browserhelper;
import com.trainerwebchecklist.qa.testdata.LoginDataProvider;
import com.trainerwebchecklist.qa.utilities.Log;




public class ProdLoginTest {

	static WebDriver driver;
	
@BeforeClass
public void OpenBrowser() throws Exception {	
	PropertyConfigurator.configure("log4j.properties");
	driver = Browserhelper.openProdBrowser();}

@Test (dataProvider = "ProdUserLogin" , dataProviderClass = LoginDataProvider.class)
public void ProductionTrainerLogin(String Username ,String Password) throws Exception{
	LoginAction.trainerProdLogin(driver, Username, Password);
  //  LogoutAction.UserLogOut(driver);
    }

@AfterClass
public void teardown() throws Exception{	
	 CloseBrowserAction.Application(driver);}
}