package testcases;

import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.trainerwebchecklist.qa.actions.CloseBrowserAction;
import com.trainerwebchecklist.qa.actions.FacebookAction;
import com.trainerwebchecklist.qa.helpers.Browserhelper;
import com.trainerwebchecklist.qa.testdata.LoginDataProvider;



public class ProdFacebookLoginTest {

	static WebDriver driver;
	
@BeforeClass
public void OpenBrowser() throws Exception {	
	PropertyConfigurator.configure("log4j.properties");
	driver = Browserhelper.openProdBrowser();}
	
@Test (dataProvider = "FacebookLogin" , dataProviderClass = LoginDataProvider.class)
public static void ProductionFacebookLogin(String Facebookid,String FacebookPswd) throws Exception{	
	FacebookAction.socialFacebookProdLogin(driver, Facebookid, FacebookPswd);
	//LogoutAction.UserLogOut(driver);
	}

@AfterClass
public void teardown() throws Exception{	
	CloseBrowserAction.Application(driver);}
}

