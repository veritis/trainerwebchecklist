package testcases;

import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.trainerwebchecklist.qa.actions.CloseBrowserAction;
import com.trainerwebchecklist.qa.actions.FacebookAction;
import com.trainerwebchecklist.qa.helpers.Browserhelper;
import com.trainerwebchecklist.qa.testdata.LoginDataProvider;
import com.trainerwebchecklist.qa.utilities.Log;



public class FacebookLoginTest {

	static WebDriver driver;
	
@BeforeClass
public void TrainerSocialBrowser() throws Exception {	
	PropertyConfigurator.configure("log4j.properties");
	driver = Browserhelper.openBrowser();}	

@Test (dataProvider = "FacebookLogin" , dataProviderClass = LoginDataProvider.class)
public static void FacebookLogin(String Facebookid,String FacebookPswd) throws Exception{	
	FacebookAction.socialFacebookLogin(driver, Facebookid, FacebookPswd);
	//LogoutAction.UserLogOut(driver);
	}

@AfterClass
public void teardown() throws Exception{
	CloseBrowserAction.Application(driver);}
}

