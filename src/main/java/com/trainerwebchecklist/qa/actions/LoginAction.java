package com.trainerwebchecklist.qa.actions;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.trainerwebchecklist.qa.pages.LoginPage;
import com.trainerwebchecklist.qa.utilities.Log;



public class LoginAction {

	static WebDriver driver;
	
	public static void trainerLogin(WebDriver driver ,String Uname ,String Pswd) throws Exception{
		
		LoginPage login = new LoginPage(driver);
		
		login.TrainerUsername.isDisplayed();
		login.TrainerUsername.clear();
		login.TrainerUsername.sendKeys(Uname);	
		Thread.sleep(2000);
		Log.info("Entered Trainer valid username");
		
		login.TrainerPassword.isDisplayed();
		login.TrainerPassword.clear();
		login.TrainerPassword.sendKeys(Pswd);
		Thread.sleep(2000);
		Log.info("Entered Trainer valid Password");
		
		login.TrainerLoginBtn.isDisplayed();
		login.TrainerLoginBtn.isEnabled();	
		login.TrainerLoginBtn.click();	
		Thread.sleep(7000);
		
		Assert.assertEquals(driver.getCurrentUrl(), "https://qa.fitbase.com/trainer/#/dashboard");
		Log.info("Succesfully vaidated dashboard page");		
		Thread.sleep(2000);
		Log.info("Successfully validated Login functionality with valid inputs");
	}
	
public static void trainerProdLogin(WebDriver driver ,String Uname ,String Pswd) throws Exception{
		
		LoginPage login = new LoginPage(driver);
		
		login.TrainerUsername.isDisplayed();
		login.TrainerUsername.clear();
		login.TrainerUsername.sendKeys(Uname);	
		Thread.sleep(2000);
		Log.info("Entered Trainer valid username");
		
		login.TrainerPassword.isDisplayed();
		login.TrainerPassword.clear();
		login.TrainerPassword.sendKeys(Pswd);
		Thread.sleep(2000);
		Log.info("Entered Trainer valid Password");
		
		login.TrainerLoginBtn.isDisplayed();
		login.TrainerLoginBtn.isEnabled();	
		login.TrainerLoginBtn.click();	
		Thread.sleep(7000);
		
		Assert.assertEquals(driver.getCurrentUrl(), "https://www.fitbase.com/trainer/#/dashboard");
		Log.info("Succesfully vaidated dashboard page");		
		Thread.sleep(2000);
		Log.info("Successfully validated Production Normal functionality with valid inputs");
	}
}

