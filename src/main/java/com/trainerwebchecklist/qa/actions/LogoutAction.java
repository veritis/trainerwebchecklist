package com.trainerwebchecklist.qa.actions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

import com.trainerwebchecklist.qa.pages.LogOutPage;
import com.trainerwebchecklist.qa.utilities.Log;



public class LogoutAction {

public static void UserLogOut(WebDriver driver) throws Exception{	
	
	@SuppressWarnings("unused")
	LogOutPage logout = new LogOutPage(driver);
			
	Actions action = new Actions(driver);
	action.moveToElement(LogOutPage.ClickonProfilebutton).click().build().perform();
	Thread.sleep(2000);	
		
	LogOutPage.TrainerLogout.click();
	Thread.sleep(5000);
	Log.info("succesfully LogOut from Application");
	}
}


