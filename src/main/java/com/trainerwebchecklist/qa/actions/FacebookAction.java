package com.trainerwebchecklist.qa.actions;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.trainerwebchecklist.qa.helpers.WindowsHandlehelper;
import com.trainerwebchecklist.qa.pages.SocialLoginPage;
import com.trainerwebchecklist.qa.utilities.Log;

public class FacebookAction {

	static WebDriver driver;
	
public static void socialFacebookLogin(WebDriver driver ,String Facebookid,String FacebookPswd) throws Exception{		
	SocialLoginPage Facebook = new SocialLoginPage(driver);
	
	Facebook.ContinueWithFacebook.isDisplayed();
	Facebook.ContinueWithFacebook.isEnabled();
	Facebook.ContinueWithFacebook.click();
	Thread.sleep(2000);
	Log.info("Click action performed on Continue with Facebook.");
	
	WindowsHandlehelper.WindowsHandle(driver, 2);
	
	Facebook.FacebookUserName.isDisplayed();
	Facebook.FacebookUserName.clear();
	Facebook.FacebookUserName.sendKeys(Facebookid);
	Log.info("Facebook valid id is enetered in ID edit field.");
		
	Facebook.FacebookUserPassword.isDisplayed();
	Facebook.FacebookUserPassword.clear();
	Facebook.FacebookUserPassword.sendKeys(FacebookPswd);
	Thread.sleep(5000);
	Log.info("FacebooK valid password is enetered in password edit field.");
		
	Facebook.ClickonFBloginbutton.isDisplayed();
	Facebook.ClickonFBloginbutton.isEnabled();
	Facebook.ClickonFBloginbutton.click();	

//	driver.findElement(By.xpath("//*[@id='platformDialogForm']/div[3]/div/table/tbody/tr/td[2]/button[1]")).click();
	Thread.sleep(16000);
	
	WindowsHandlehelper.WindowsHandle(driver, 1);
	Log.info("Click action performed to login button.");
	
	Thread.sleep(7000);

	Assert.assertEquals(driver.getCurrentUrl(), "https://qa.fitbase.com/trainer/#/dashboard");
	Log.info("Succesfully vaidated dashboard page");		
	Thread.sleep(2000);
	Log.info("Successfully validated Facebook Login functionality with valid inputs");
 }

public static void socialFacebookProdLogin(WebDriver driver ,String Facebookid,String FacebookPswd) throws Exception{		
	SocialLoginPage Facebook = new SocialLoginPage(driver);
	
	Facebook.ContinueWithFacebook.isDisplayed();
	Facebook.ContinueWithFacebook.isEnabled();
	Facebook.ContinueWithFacebook.click();
	Thread.sleep(2000);
	Log.info("Click action performed on Continue with Facebook.");
	
	WindowsHandlehelper.WindowsHandle(driver, 2);
	
	Facebook.FacebookUserName.isDisplayed();
	Facebook.FacebookUserName.clear();
	Facebook.FacebookUserName.sendKeys(Facebookid);
	Log.info("Facebook valid id is enetered in ID edit field.");
		
	Facebook.FacebookUserPassword.isDisplayed();
	Facebook.FacebookUserPassword.clear();
	Facebook.FacebookUserPassword.sendKeys(FacebookPswd);
	Thread.sleep(5000);
	Log.info("FacebooK valid password is enetered in password edit field.");
		
	Facebook.ClickonFBloginbutton.isDisplayed();
	Facebook.ClickonFBloginbutton.isEnabled();
	Facebook.ClickonFBloginbutton.click();	

//	driver.findElement(By.xpath("//*[@id='platformDialogForm']/div[3]/div/table/tbody/tr/td[2]/button[1]")).click();
	Thread.sleep(16000);
	
	WindowsHandlehelper.WindowsHandle(driver, 1);
	Log.info("Click action performed to login button.");
	
	Thread.sleep(8000);

	Assert.assertEquals(driver.getCurrentUrl(), "https://www.fitbase.com/trainer/#/dashboard");
	Log.info("Succesfully vaidated dashboard page");		
	Thread.sleep(2000);
	Log.info("Successfully validated Production Facebook Login functionality with valid inputs");
 }
}
