package com.trainerwebchecklist.qa.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class ReadConfig {
	
	Properties prop;

public ReadConfig() {
		
		File file = new File("./src/main/java/com/trainerweb/qa/configuration/config.properties");
	
   try {
		FileInputStream input =new FileInputStream(file);
		prop = new Properties();
		prop.load(input);
		
	  } catch(Exception e) {
		System.out.println("Exception is "+e.getMessage());}
  }

public String getapplicationURL() {
	 String URL = prop.getProperty("BaseURL");
	 return URL;}

public String browser() {
	String browser = prop.getProperty("browser");
	 return browser;}

public String Chromepath() {
	 String Chromepath = prop.getProperty("Chromepath");
	 return Chromepath;}

public String Mozillapath() {
	 String Mozillapath = prop.getProperty("Mozillapath");
	 return Mozillapath;}

public String iepath() {
	 String iepath = prop.getProperty("iepath");
	 return iepath;}

//To check mail for Forgot Password Link
public String getMailinatorURL() {
	 String URL1 = prop.getProperty("Mail");
	 return URL1;}
}